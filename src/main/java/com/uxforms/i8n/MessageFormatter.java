package com.uxforms.i8n;

import com.ibm.icu.text.MessageFormat;

import java.util.Locale;
import java.util.Map;

/**
 * Delegates to {@link com.ibm.icu.text.MessageFormat}
 */
public class MessageFormatter {

    private final Locale locale;

    public MessageFormatter(Locale locale) {
        this.locale = locale;
    }

    public String format(String toBeFormatted, Object[] args) {
        return new MessageFormat(toBeFormatted, locale).format(args, new StringBuffer(), null).toString();
    }

    public String format(String toBeFormatted, Map<String, Object> args) {
        return new MessageFormat(toBeFormatted, locale).format(args, new StringBuffer(), null).toString();
    }
}
