package com.uxforms.i8n;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Properties that:
 * <ul>
 *     <li>allows duplicate keys</li>
 *     <li>preserves insertion order</li>
 * </ul>
 */
public class OrderedProperties extends Properties {

    private final List<Entry> data = new ArrayList<>();

    @Override
    public Object put(Object key, Object value) {
        data.add(new Entry(key, value));
        return null;
    }

    public Object put(Entry entry) {
        data.add(entry);
        return null;
    }

    @Override
    public Object get(Object key) {
        Optional<Object> found = data.stream()
                .filter(e -> e.key.equals(key))
                .map(e -> e.value)
                .findFirst();
        if (found.isPresent()) {
            return found.get();
        } else {
            return null;
        }
    }

    public List<Entry> allEntries() {
        return Collections.unmodifiableList(data);
    }

    @Override
    public Enumeration<Object> keys() {
        return Collections.enumeration(data.stream().map(e -> e.key).collect(Collectors.toList()));
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }


}

