package com.uxforms.i8n;

import java.nio.charset.StandardCharsets;

public class UTF8SourceOrderControl extends SourceOrderControl {

    public UTF8SourceOrderControl(MessageFormatter formatter) {
        super(formatter, StandardCharsets.UTF_8);
    }
}
