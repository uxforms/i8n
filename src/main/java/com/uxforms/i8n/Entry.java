package com.uxforms.i8n;

public class Entry {
    final public Object key;
    final public Object value;

    public Entry(Object key, Object value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[key=" + key.toString() + ",value=" + key.toString() + "]";
    }
}
