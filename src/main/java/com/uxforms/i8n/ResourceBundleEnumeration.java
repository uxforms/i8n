package com.uxforms.i8n;

import java.util.*;

/**
 * Copied from {@link sun.util.ResourceBundleEnumeration} to avoid having to provide this package
 * to the OSGi container.
 */
public class ResourceBundleEnumeration implements Enumeration<String> {

    List<String> list;
    Iterator<String> iterator;
    Enumeration<String> enumeration;
    String next = null;

    public ResourceBundleEnumeration(List<String> var1, Enumeration<String> var2) {
        this.list = var1;
        this.iterator = var1.iterator();
        this.enumeration = var2;
    }

    public boolean hasMoreElements() {
        if(this.next == null) {
            if(this.iterator.hasNext()) {
                this.next = (String)this.iterator.next();
            } else if(this.enumeration != null) {
                while(this.next == null && this.enumeration.hasMoreElements()) {
                    this.next = (String)this.enumeration.nextElement();
                    if(this.list.contains(this.next)) {
                        this.next = null;
                    }
                }
            }
        }

        return this.next != null;
    }

    public String nextElement() {
        if(this.hasMoreElements()) {
            String var1 = this.next;
            this.next = null;
            return var1;
        } else {
            throw new NoSuchElementException();
        }
    }
}
