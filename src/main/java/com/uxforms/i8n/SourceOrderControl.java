package com.uxforms.i8n;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Locale;
import java.util.ResourceBundle;

public class SourceOrderControl extends ResourceBundle.Control {

    protected final MessageFormatter formatter;
    protected final Charset charset;

    public SourceOrderControl(MessageFormatter formatter) {
        this(formatter, StandardCharsets.ISO_8859_1);
    }

    protected SourceOrderControl(MessageFormatter formatter, Charset charset) {
        this.formatter = formatter;
        this.charset = charset;
    }

    @Override
    public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException {

        if (!format.equals("java.properties")) {
            return super.newBundle(baseName, locale, format, loader, reload);
        }

        final String resourceName = toResourceName(toBundleName(baseName, locale), "properties");
        final Boolean reloadFlag = reload;
        final ClassLoader classLoader = loader;
        InputStream stream = null;
        try {
            stream = AccessController.doPrivileged(
                    (PrivilegedExceptionAction<InputStream>) () -> {
                        InputStream is = null;
                        if (reloadFlag) {
                            URL resource = classLoader.getResource(resourceName);
                            if (resource != null) {
                                URLConnection urlConnection = resource.openConnection();
                                if (urlConnection != null) {
                                    urlConnection.setUseCaches(false);
                                    is = urlConnection.getInputStream();
                                }
                            }
                        } else {
                            is = classLoader.getResourceAsStream(resourceName);
                        }
                        return is;
                    }
            );
        } catch (PrivilegedActionException pae) {
            throw (IOException) pae.getException();
        }

        OrderedResourceBundle bundle = null;
        if (stream != null) {
            try {
                bundle = new OrderedResourceBundle(stream, formatter, charset);
            } finally {
                stream.close();
            }
        }
        return bundle;

    }
}
