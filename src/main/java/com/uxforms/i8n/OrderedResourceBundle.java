package com.uxforms.i8n;

import com.ibm.icu.text.MessageFormat;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

public class OrderedResourceBundle extends ResourceBundle {

    private final OrderedProperties data;
    private final MessageFormatter formatter;

    public static OrderedResourceBundle getOrderedBundle(String baseName, Locale locale, ClassLoader classLoader) {
        return (OrderedResourceBundle) getBundle(baseName, locale, classLoader, new SourceOrderControl(new MessageFormatter(locale)));
    }

    public static OrderedResourceBundle getUTF8OrderedBundle(String baseName, Locale locale, ClassLoader classLoader) {
        return (OrderedResourceBundle) getBundle(baseName, locale, classLoader, new UTF8SourceOrderControl(new MessageFormatter(locale)));
    }

    public static OrderedResourceBundle getOrderedBundle(String baseName, Locale locale, ClassLoader classLoader, MessageFormatter formatter) {
        return (OrderedResourceBundle) getBundle(baseName, locale, classLoader, new SourceOrderControl(formatter));
    }

    public static OrderedResourceBundle getUTF8OrderedBundle(String baseName, Locale locale, ClassLoader classLoader, MessageFormatter formatter) {
        return (OrderedResourceBundle) getBundle(baseName, locale, classLoader, new UTF8SourceOrderControl(formatter));
    }

    public OrderedResourceBundle(InputStream is, MessageFormatter formatter) throws IOException {
        data = new OrderedProperties();
        data.load(is);
        this.formatter = formatter;
    }

    public OrderedResourceBundle(InputStream is, MessageFormatter formatter, Charset charset) throws IOException {
        data = new OrderedProperties();
        data.load(new InputStreamReader(is, charset));
        this.formatter = formatter;
    }

    private OrderedResourceBundle(OrderedProperties properties, MessageFormatter formatter) {
        this.data = properties;
        this.formatter = formatter;
    }

    @Override
    protected Object handleGetObject(String key) {
        return data.get(key);
    }

    /**
     * Crucially, the enumeration returns keys <i>in the same order in which they are present in the source properties file</i>.
     * This means that drop-down or select options can be stored in properties files as not only the display value is localised, but so can be the <i>order</i>
     * in which items are presented.
     *
     * @return An enumeration of all keys present, in the same order as which they appear in the source properties file.
     */
    @Override
    public Enumeration<String> getKeys() {
        List<String> myKeys = Collections.list(data.keys()).stream().map(k -> (String) k).collect(Collectors.toList());
        return new ResourceBundleEnumeration(myKeys, Optional.ofNullable(this.parent).map(ResourceBundle::getKeys).orElse(null));
    }

    public Iterator<Entry> getEntries() {
        return data.allEntries().iterator();
    }

    @Override
    public Set<String> keySet() {
        Set<String> keys = new LinkedHashSet<>();
        for (OrderedResourceBundle rb = this; rb != null; rb = (OrderedResourceBundle) rb.parent) {
            rb.handleKeySet().stream().forEach(keys::add);
        }
        return keys;
    }

    @Override
    protected Set<String> handleKeySet() {
        Set<String> keys = new LinkedHashSet<>();
        data.allEntries().forEach(e -> keys.add((String) e.key));
        return keys;
    }

    /**
     * Does not apply any formatting to the associated value.
     * <p>
     * Use this method when the message does not have named parameters, or you need to
     * pass the formatting instructions through un-processed.
     *
     * @param key bundle property key
     * @return The string without any formatting applied
     * @throws MissingResourceException if there is no message for the given key
     */
    public String getUnformattedString(String key) {
        return getString(key);
    }

    /**
     * Does not apply any formatting to the associated value.
     * <p>
     * Use this method when the message does not have named parameters, or you need to
     * pass the formatting instructions through un-processed.
     *
     * @param key bundle property key
     * @return {@link Optional#empty()} if there is no message for the given key.
     * @see #getUnformattedString(String)
     */
    public Optional<String> getUnformattedStringOption(String key) {
        try {
            return Optional.ofNullable(getUnformattedString(key));
        } catch (MissingResourceException mre) {
            return Optional.empty();
        }
    }

    /**
     * Formats the associated value according to {@link MessageFormat}.
     * <p>
     * Use this method when the message has named parameters.
     *
     * @param key  bundle property key
     * @param args named parameters: name -&gt; value
     * @return The formatted string
     * @throws MissingResourceException if there is no message for the given key
     */
    public String getString(String key, Map<String, Object> args) {
        return formatter.format(getString(key), args);
    }

    /**
     * @param key  bundle property key
     * @param args named parameters: name -&gt; value
     * @return {@link Optional#empty()} if there is no message for the given key.
     * @see #getString(String, Map)
     */
    public Optional<String> getStringOption(String key, Map<String, Object> args) {
        try {
            return Optional.ofNullable(getString(key, args));
        } catch (MissingResourceException mre) {
            return Optional.empty();
        }
    }

    /**
     * Formats the associated value according to {@link MessageFormat}.
     * <p>
     * Use this method when the message has numbered parameters.
     *
     * @param key  bundle property key
     * @param args ordered parameters
     * @return The formatted string
     */
    public String getString(String key, Object[] args) {
        return formatter.format(getString(key), args);
    }

    /**
     * @param key  bundle property key
     * @param args ordered parameters
     * @return {@link Optional#empty()} if there is no message for the given key.
     * @see #getString(String, Object[])
     */
    public Optional<String> getStringOption(String key, Object[] args) {
        try {
            return Optional.ofNullable(getString(key, args));
        } catch (MissingResourceException mre) {
            return Optional.empty();
        }
    }

    public OrderedResourceBundle addAll(OrderedResourceBundle bundleToAdd) {
        OrderedProperties added = new OrderedProperties();

        this.data.allEntries().forEach(added::put);
        bundleToAdd.data.allEntries().forEach(added::put);
        return new OrderedResourceBundle(added, this.formatter);
    }
}
