package com.uxforms.i8n;

import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

public class OrderedPropertiesTest {

    @Test
    public void putAndKeysShouldAllowDuplicateEntries() {
        OrderedProperties op = new OrderedProperties();
        op.put("key", "value");
        op.put("key", "value");
        assertThat(Collections.list(op.keys()), hasSize(2));
    }

    @Test
    public void putShouldAlwaysReturnNull() {
        OrderedProperties op = new OrderedProperties();
        assertThat(op.put("key", "value"), is(nullValue()));
        assertThat("even when put is called again for the same key value",
                op.put("key", "another value"), is(nullValue()));
    }

    @Test
    public void getShouldReturnNullIfThereIsNoSuchKey() {
        OrderedProperties op = new OrderedProperties();
        assertThat(op.get("whoops"), is(nullValue()));
    }

    @Test
    public void isEmpty() {
        OrderedProperties op = new OrderedProperties();
        assertThat("empty", op.isEmpty(), is(true));
        op.put("key", "value");
        assertThat("non-empty", op.isEmpty(), is(false));
    }

    @Test
    public void size() {
        OrderedProperties op = new OrderedProperties();
        assertThat("empty", op.size(), is(equalTo(0)));
        op.put("key", "value");
        assertThat("one entry", op.size(), is(equalTo(1)));
        op.put("anotherKey", "anotherValue");
        assertThat("two entries", op.size(), is(equalTo(2)));
    }
}