package com.uxforms.i8n;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.lessThan;

public class OrderedResourceBundleTest {

    Locale BRITAIN = new Locale("en", "GB");
    Locale DENMARK = new Locale("da", "DK");
    ClassLoader classLoader = getClass().getClassLoader();

    OrderedResourceBundle britishMessages = OrderedResourceBundle.getOrderedBundle("messages", BRITAIN, classLoader);
    OrderedResourceBundle danishMessages = OrderedResourceBundle.getOrderedBundle("messages", DENMARK, classLoader);

    @Test
    public void shouldLoadMessagesViaResourceBundle() {
        ResourceBundle messages = ResourceBundle.getBundle("messages", BRITAIN, new SourceOrderControl(new MessageFormatter(BRITAIN)));
        assertThat(messages.keySet(), hasItem("keyOne"));
    }

    @Test
    public void shouldLoadDifferentMessageDependingUponLocale() {
        ResourceBundle otherLocale = ResourceBundle.getBundle("messages", DENMARK, new SourceOrderControl(new MessageFormatter(DENMARK)));
        assertThat(otherLocale.getString("keyOne"), equalTo("Hej"));
    }

    @Test
    public void getKeysShouldPreserveTheOrderOfKeysInOneLocaleFile() {
        Enumeration<String> keys = britishMessages.getKeys();
        assertThat(keys.nextElement(), equalTo("keyOne"));
        assertThat(keys.nextElement(), equalTo("keyTwo"));
    }

    @Test
    public void getKeysShouldPreserveTheDifferentOrderOfKeysInAnotherLocaleFile() {
        Enumeration<String> keys = danishMessages.getKeys();
        assertThat(keys.nextElement(), equalTo("keyTwo"));
        assertThat(keys.nextElement(), equalTo("keyOne"));
    }

    @Test
    public void keySetShouldPreserveTheOrderOfKeysInOneLocaleFile() {
        Iterator<String> keys = britishMessages.keySet().iterator();
        assertThat(keys.next(), equalTo("keyOne"));
        assertThat(keys.next(), equalTo("keyTwo"));
    }

    @Test
    public void keySetShouldPreserveTheOrderOfKeysInAnotherLocaleFile() {
        Iterator<String> keys = danishMessages.keySet().iterator();
        assertThat(keys.next(), equalTo("keyTwo"));
        assertThat(keys.next(), equalTo("keyOne"));
    }

    @Test
    public void shouldApplyFormattingToMessageWithNamedPlaceholders() {
        OrderedResourceBundle bundle = OrderedResourceBundle.getOrderedBundle("messages", BRITAIN, classLoader);
        Map<String, Object> args = new HashMap<>();
        args.put("GENDER", "male");
        args.put("ResCount", 1);
        args.put("CatCount", 5);
        String result = bundle.getString("keyWithNamedValues", args);
        assertThat(result, equalTo("He just found one result in 5 categories."));
    }

    @Test
    public void shouldApplyFormattingToMessageWithNumberedPlaceholders() {
        OrderedResourceBundle bundle = OrderedResourceBundle.getOrderedBundle("messages", BRITAIN, classLoader);
        LocalDateTime ldt = LocalDateTime.of(LocalDate.of(2015, 3, 20), LocalTime.of(5, 40, 30));
        Object[] args = {7, Date.from(ldt.toInstant(ZoneOffset.UTC)), "a disturbance in the Force"};

        String result = bundle.getString("keyWithNumberedValues", args);
        assertThat(result, equalTo("At 05:40:30 on 20 March 2015, there was a disturbance in the Force on planet 7."));
    }

    @Test
    public void shouldApplyLocaleAwareFormatting() {
        OrderedResourceBundle british = OrderedResourceBundle.getOrderedBundle("messages", BRITAIN, classLoader);
        String britishFormat = british.getString("keyWithCurrency", new Object[] {3.80});
        assertThat(britishFormat, equalTo("This costs £3.80"));

        OrderedResourceBundle danish = OrderedResourceBundle.getOrderedBundle("messages", DENMARK, classLoader);
        String danishFormat = danish.getString("keyWithCurrency", new Object[]{3.80});
        assertThat(danishFormat, equalTo("This costs 3,80\u00A0kr."));
    }

    @Test(expected = MissingResourceException.class)
    public void nonExistentKeyForGetStringShouldThrowException() {
        britishMessages.getString("whoops");
    }

    @Test
    public void nonExistentKeyForGetStringOptionShouldReturnNone() {
        assertThat(britishMessages.getStringOption("whoops", new HashMap<>()), equalTo(Optional.empty()));
        assertThat(britishMessages.getStringOption("whoops", new Object[]{}), equalTo(Optional.empty()));
    }

    @Test
    public void getKeysIncludeKeysFromRootPropertiesEvenIfNotPresentInLocale() {
        List<String> danishKeys = Collections.list(danishMessages.getKeys());
        List<String> britishKeys = Collections.list(britishMessages.getKeys());

        assertThat("different number of keys in each bundle", danishMessages.handleKeySet().size(), lessThan(britishMessages.handleKeySet().size()));

        assertThat("same number of keys in enumeration", danishKeys.size(), equalTo(britishKeys.size()));
    }

    @Test
    public void addAllShouldAppendTheGivenBundleToThisOne() {
        List<String> danishKeys = new ArrayList<>(danishMessages.handleKeySet());
        List<String> britishKeys = new ArrayList<>(britishMessages.handleKeySet());

        OrderedResourceBundle appended = britishMessages.addAll(danishMessages);
        List<String> appendedKeys = Collections.list(appended.getKeys());

        assertThat(appendedKeys.size(), equalTo(britishKeys.size() + danishKeys.size()));
        assertThat(appendedKeys.get(0), equalTo(britishKeys.get(0)));
        assertThat(appendedKeys.get(appendedKeys.size() - 1), equalTo(danishKeys.get(danishKeys.size() - 1)));
    }

    @Test
    public void shouldProvideWayToLoadPropertiesInUTF8() {
        assertThat(OrderedResourceBundle.getUTF8OrderedBundle("utf8", Locale.UK, classLoader).getString("arabic"),
                equalTo("‫‫‫المدعي‬ ‫بيـانات‬"));
    }

    @Test
    public void getUnformattedStringShouldPassFormattingThroughUntouched() {
        assertThat(britishMessages.getUnformattedString("keyWithCurrency"), equalTo("This costs {0,number,currency}"));
    }

    @Test
    public void getUnformattedStringOptionShouldPassFormattingThroughUntouched() {
        assertThat(britishMessages.getUnformattedStringOption("keyWithCurrency"), equalTo(Optional.of("This costs {0,number,currency}")));
    }

    @Test(expected = MissingResourceException.class)
    public void nonExistentKeyForGetUnformattedStringShouldThrowException() {
        britishMessages.getUnformattedString("whoops");
    }

    @Test
    public void nonExistentKeyForGetUnformattedStringOptionShouldReturnNone() {
        assertThat(britishMessages.getUnformattedStringOption("whoops"), equalTo(Optional.empty()));
    }

}
