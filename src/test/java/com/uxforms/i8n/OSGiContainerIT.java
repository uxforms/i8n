package com.uxforms.i8n;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerSuite;
import org.ops4j.pax.exam.util.PathUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.ops4j.pax.exam.CoreOptions.*;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerSuite.class)
public class OSGiContainerIT {

    @Inject
    private BundleContext context;

    @Configuration
    public Option[] config() {

        return options(
                mavenBundle("com.ibm.icu", "icu4j", "67.1"),
                mavenBundle("org.slf4j", "slf4j-api", "1.7.25"),
                mavenBundle("ch.qos.logback", "logback-core", "1.2.3"),
                mavenBundle("ch.qos.logback", "logback-classic", "1.2.3"),
                systemProperty("logback.configurationFile")
                        .value("file:" + PathUtils.getBaseDir() + "/src/test/resources/logback-test.xml"),
                bundle("reference:file:target/classes"),
                junitBundles(),
                cleanCaches()
        );
    }

    @Test
    public void i8nDeployedSuccessfullyInOSGiContainer() {
        Bundle[] bundles = context.getBundles();
        Optional<Bundle> deployedi8n = Arrays.asList(bundles).stream().filter(b -> "com.uxforms.i8n".equals(b.getSymbolicName())).findFirst();
        Bundle deployed = deployedi8n.get();
        assertThat(deployed.getState(), equalTo(Bundle.ACTIVE));
    }
}
