# UX Forms i8n

A library for working with localised resource bundles.

This library allows you to work with internationalised resource bundles, backed by properties files, where the order of keys is preserved
from the source file. 
 
## Usage

Either `ResourceBundle.getBundle("baseName", myLocale, new OrderedProperties(myLocale))` to get a standard `ResourceBundle`, or 
 `OrderedResourceBundle.getOrderedBundle("messages", myLocale)` to get an instance of `OrderedResourceBundle`.
 

OrderedResourceBundle also has two additional methods which make applying string formatting to property values very easy.

`#.getString(keyName, Map<String, Object>)`

Use this method when the format string contains named placeholders. E.g. `{GENDER,select, male {He} female {She} other {They}}`

`#.getString(keyName, Object[])`

Use this method when the format string contains numbered placeholders. E.g. `This costs {0,number,currency}`

String formats are processed by the [ICU MessageFormat class](http://www.icu-project.org/apiref/icu4j/com/ibm/icu/text/MessageFormat.html).

There is an article [explaining the benefits of this over Java's own MessageFormat implementation](https://alexsexton.com/blog/2012/03/the-ux-of-language/), 
which provides a good example of the sort of readable formats supported by this library:

```
{GENDER, select,
  male {He}
  female {She}
  other {They}
} just found {ResCount, plural,
        one {one result}
        other {# results}
      } in {CatCount, plural,
        one {one category}
        other {# categories}
      }.
```

## Developing

Run integration tests: `mvn verify`

Release a new version: `mvn release:prepare` then `mvn release:perform`
